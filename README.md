# Wefox

Repository for wefox frontend challenge

## Getting Started

### Server API

Pull docker image

```
  docker pull wefoxgroup/wg-web-challenge:latest
```

In root of project run 

```
  docker-compose up --no-start
```

Followed by
```
  docker-compose up
```

The application will now be listening on port 3000

### Angular App

Install dependencies

```
  npm install
```

Compile app

```
  npm build
```

Start server

```
  npm start
```

Front end will now be running at http://localhost:4200

### Tests

```
  npm run test
```

