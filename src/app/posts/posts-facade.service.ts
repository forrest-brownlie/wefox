import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PostsState } from '@posts/posts.reducers';
import { getPosts, getShowPost } from '@posts/posts.selectors';
import { PostsActions } from '@posts/posts.actions';
import { Post, NewPost } from '@posts/posts.model';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';

@Injectable()
export class PostsFacadeService {
  posts$: Observable<Post[]> = this.store.pipe(select(getPosts));
  showPost$: Observable<Post> = this.store.pipe(select(getShowPost));

  constructor(private store: Store<PostsState>) { }

  loadPosts(): void {
    this.store.dispatch(PostsActions.listPosts());
  }

  updatePost(post: Post, drawerRef: NzDrawerRef): void {
    const { id: postId } = post;
    const closeDrawer = () => drawerRef.close();
    this.store.dispatch(PostsActions.updatePost({ post, postId, closeDrawer }));
  }

  createPost(post: NewPost, drawerRef: NzDrawerRef): void {
    const closeDrawer = () => drawerRef.close();
    this.store.dispatch(PostsActions.createPost({ post, closeDrawer }));
  }

  removePost(postId: number): void {
    this.store.dispatch(PostsActions.removePost({ postId }));
  }

  showPost(postId: number): void {
    this.store.dispatch(PostsActions.showPost({ postId }));
  }

  removeShowPost(): void {
    this.store.dispatch(PostsActions.removeShowPost());
  }
}
