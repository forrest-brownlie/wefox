import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { PostsFacadeService } from '@posts/posts-facade.service';
import { postsStub } from '../test-util/posts.stub';
import { initialState, PostsState } from '@posts/posts.reducers';
import { getPosts } from '@posts/posts.selectors';

describe('Posts Facade Service', () => {
  let postsFacadeService: PostsFacadeService;
  let store: MockStore;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PostsFacadeService,
        provideMockStore({
          initialState: {},
          selectors: [
            {
              selector: getPosts,
              value: postsStub
            }
          ]
        })
      ]
    });
    postsFacadeService = TestBed.inject(PostsFacadeService);
    store = TestBed.inject(MockStore);
  });

  it('should return list of posts', done => {
    const dummyState: PostsState = {
      ...initialState,
      list: postsStub,
    };
    store.setState(dummyState);
    postsFacadeService.posts$.subscribe(posts => {
      expect(posts).toEqual(dummyState?.list);
      done();
    });
  });
});
