import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import dayjs from 'dayjs';
import { NewPost, Post } from '@posts/posts.model';

@Injectable()
export class PostFormService {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      lat: '',
      long: '',
      imageUrl: '',
      createdAt: '',
      updatedAt: '',
    });
  }

  submit(post: Post): Post | NewPost {
    const dateNow = dayjs().format();
    const id = post?.id;
    const formValue = this.form.value;
    if (id) {
      return {
        id,
        updatedAt: dateNow,
        ...formValue
      };
    }
    return {
      createdAt: dateNow,
      ...formValue,
    };
  }
}
