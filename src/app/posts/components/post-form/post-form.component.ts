import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Post, NewPost } from '@posts/posts.model';
import { PostFormService } from '@posts/components/post-form/post-form.service';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss'],
  viewProviders: [PostFormService]
})
export class PostFormComponent implements OnInit {
  @Input() post: Post;
  @Output() updatePost: EventEmitter<Post> = new EventEmitter<Post>();
  @Output() createPost: EventEmitter<NewPost> = new EventEmitter<NewPost>();

  get form(): FormGroup {
    return this.postFormService.form;
  }

  constructor(private postFormService: PostFormService) { }

  ngOnInit(): void {
    if (this.post) {
      this.postFormService.form.patchValue(this.post);
    }
  }

  submit(): void {
    if (!this.form.valid) {
      return null;
    }
    const postData = this.postFormService.submit(this.post);
    if ('id' in postData) {
      return this.updatePost.emit(postData);
    }
    return this.createPost.emit(postData);
  }
}
