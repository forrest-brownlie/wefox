import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostFormComponent } from './post-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { singlePostStub } from '../../../test-util/posts.stub';
import { By } from '@angular/platform-browser';

const getInputValue = (id: string, fixture: ComponentFixture<PostFormComponent>) => {
  const input = fixture.debugElement.query(By.css(`#${id}`));
  return input.nativeElement.value;
};

describe('PostFormComponent', () => {
  let component: PostFormComponent;
  let fixture: ComponentFixture<PostFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        NzFormModule,
        NzButtonModule,
      ],
      declarations: [ PostFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostFormComponent);
    component = fixture.componentInstance;
    component.post = singlePostStub;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('should patch the form values of Post input exists', () => {
    it('should patch the title value', () => {
      const { title } = singlePostStub;
      const value = getInputValue('title', fixture);
      expect(value).toBe(title);
    });
    it('should patch the content value', () => {
      const { content } = singlePostStub;
      const value = getInputValue('content', fixture);
      expect(value).toBe(content);
    });
    it('should patch the lat value', () => {
      const { lat } = singlePostStub;
      const value = getInputValue('lat', fixture);
      expect(value).toBe(lat);
    });
    it('should patch the long value', () => {
      const { long } = singlePostStub;
      const value = getInputValue('long', fixture);
      expect(value).toBe(long);
    });
    it('should patch the imageUrl value', () => {
      const { imageUrl } = singlePostStub;
      const value = getInputValue('imageUrl', fixture);
      expect(value).toBe(imageUrl);
    });
  });
});
