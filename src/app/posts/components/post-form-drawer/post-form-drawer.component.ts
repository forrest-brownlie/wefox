import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NewPost, Post } from '@posts/posts.model';
import { PostsFacadeService } from '@posts/posts-facade.service';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';

@Component({
  selector: 'app-post-form-drawer',
  templateUrl: './post-form-drawer.component.html',
  styleUrls: ['./post-form-drawer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  viewProviders: [PostsFacadeService]
})
export class PostFormDrawerComponent {
  @Input() post: Post;

  constructor(
    private postsFacadeService: PostsFacadeService,
    private drawerRef: NzDrawerRef<string>
  ) { }

  createPost(post: NewPost): void {
    this.postsFacadeService.createPost(post, this.drawerRef);
  }

  updatePost(post: Post): void {
    this.postsFacadeService.updatePost(post, this.drawerRef);
  }
}
