import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostFormDrawerComponent } from './post-form-drawer.component';
import { PostFormComponent } from '@posts/components/post-form/post-form.component';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponent } from 'ng-mocks';
import { By } from '@angular/platform-browser';
import { singlePostStub } from '../../../test-util/posts.stub';

describe('PostFormDrawerComponent', () => {
  let component: PostFormDrawerComponent;
  let fixture: ComponentFixture<PostFormDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PostFormDrawerComponent,
        MockComponent(PostFormComponent)
      ],
      providers: [
        provideMockStore({})
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostFormDrawerComponent);
    component = fixture.componentInstance;
    component.post = singlePostStub;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('renders post form', () => {
    const form = fixture.debugElement.query(By.css('app-post-form'));
    expect(form).toBeTruthy();
  });

  it('passes a post input to post form', () => {
    const form = fixture.debugElement.query(By.css('app-post-form'));
    expect(form.componentInstance.post).toBe(singlePostStub);
  });
});
