import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostMapComponent } from './post-map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

describe('PostMapComponent', () => {
  let component: PostMapComponent;
  let fixture: ComponentFixture<PostMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LeafletModule],
      declarations: [ PostMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
