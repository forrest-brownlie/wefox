import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Post } from '@posts/posts.model';
import { latLng, MapOptions, tileLayer } from 'leaflet';

@Component({
  selector: 'app-post-map',
  templateUrl: './post-map.component.html',
  styleUrls: ['./post-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostMapComponent implements OnInit {
  @Input() post: Post;
  options: MapOptions;

  ngOnInit(): void {
    this.options = this.getOptions(this.post);
  }

  getOptions(post: Post): MapOptions {
    if (!post) {
      return null;
    }
    const { lat, long } = post;
    return {
      layers: [
        tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          opacity: 0.7,
          maxZoom: 19,
          detectRetina: true,
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        })
      ],
      zoom: 10,
      center: latLng(parseFloat(lat), parseFloat(long))
    };
  }
}
