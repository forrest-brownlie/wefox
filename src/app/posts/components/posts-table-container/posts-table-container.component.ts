import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '@posts/posts.model';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { PostsFacadeService } from '@posts/posts-facade.service';
import { PostFormDrawerComponent } from '@posts/components/post-form-drawer/post-form-drawer.component';

@Component({
  selector: 'app-posts-table-container',
  templateUrl: './posts-table-container.component.html',
  viewProviders: [PostsFacadeService]
})
export class PostsTableContainerComponent implements OnInit {
  posts$: Observable<Post[]>;
  showingPost$: Observable<Post>;
  readonly drawerWidth = '400px';
  showModal = false;

  constructor(
    private postsFacadeService: PostsFacadeService,
    private drawerService: NzDrawerService,
  ) {
    this.posts$ = this.postsFacadeService.posts$;
    this.showingPost$ = this.postsFacadeService.showPost$;
  }

  ngOnInit(): void {
    this.postsFacadeService.loadPosts();
  }

  editPost(post: Post): void {
    this.drawerService.create<PostFormDrawerComponent>({
      nzContent: PostFormDrawerComponent,
      nzWidth: this.drawerWidth,
      nzContentParams: { post },
    });
  }

  createPost(): void {
    this.drawerService.create<PostFormDrawerComponent>({
      nzContent: PostFormDrawerComponent,
      nzWidth: this.drawerWidth,
    });
  }

  showPost(postId: number): void {
    this.showModal = true;
    this.postsFacadeService.showPost(postId);
  }

  hideModal(): void {
    this.showModal = false;
    this.postsFacadeService.removeShowPost();
  }

  removePost({ id }: Post): void {
    this.postsFacadeService.removePost(id);
  }
}
