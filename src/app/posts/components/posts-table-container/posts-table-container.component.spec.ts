import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostsTableContainerComponent } from './posts-table-container.component';
import { provideMockStore } from '@ngrx/store/testing';
import { MockComponent } from 'ng-mocks';
import { PostsTableComponent } from '@posts/components/posts-table/posts-table.component';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';

describe('PostsTableContainerComponent', () => {
  let component: PostsTableContainerComponent;
  let fixture: ComponentFixture<PostsTableContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NzDrawerModule],
      declarations: [
        PostsTableContainerComponent,
        MockComponent(PostsTableComponent),
      ],
      providers: [provideMockStore({})],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsTableContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
