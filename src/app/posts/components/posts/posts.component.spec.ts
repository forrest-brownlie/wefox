import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostsComponent } from './posts.component';
import { PostsTableContainerComponent } from '@posts/components/posts-table-container/posts-table-container.component';
import { MockComponent } from 'ng-mocks';
import { By } from '@angular/platform-browser';

describe('PostsComponent', () => {
  let component: PostsComponent;
  let fixture: ComponentFixture<PostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PostsComponent,
        MockComponent(PostsTableContainerComponent),
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('renders posts table container', () => {
    const form = fixture.debugElement.query(By.css('app-posts-table-container'));
    expect(form).toBeTruthy();
  });
});
