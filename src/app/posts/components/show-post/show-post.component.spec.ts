import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ShowPostComponent } from './show-post.component';
import { MockComponent } from 'ng-mocks';
import { PostMapComponent } from '@posts/components/post-map/post-map.component';

describe('ShowPostComponent', () => {
  let component: ShowPostComponent;
  let fixture: ComponentFixture<ShowPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ShowPostComponent,
        MockComponent(PostMapComponent)
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
