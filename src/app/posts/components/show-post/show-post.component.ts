import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Post } from '@posts/posts.model';

@Component({
  selector: 'app-show-post',
  templateUrl: './show-post.component.html',
  styleUrls: ['./show-post.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShowPostComponent {
  @Input() post: Post;
}
