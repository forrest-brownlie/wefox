import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Post } from '@posts/posts.model';

@Component({
  selector: 'app-posts-table',
  templateUrl: './posts-table.component.html',
  styleUrls: ['./posts-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostsTableComponent {
  @Input() posts: Post[] = [];
  @Input() showingPost: Post;
  @Input() showModal: boolean;
  @Output() hideModal: EventEmitter<any> = new EventEmitter();
  @Output() editPost: EventEmitter<Post> = new EventEmitter();
  @Output() showPost: EventEmitter<number> = new EventEmitter();
  @Output() createPost: EventEmitter<any> = new EventEmitter();
  @Output() removePost: EventEmitter<Post> = new EventEmitter();
}
