import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PostsTableComponent } from './posts-table.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { MockComponent } from 'ng-mocks';
import { ShowPostComponent } from '@posts/components/show-post/show-post.component';
import { postsStub } from '../../../test-util/posts.stub';
import { By } from '@angular/platform-browser';

describe('PostsTableComponent', () => {
  let component: PostsTableComponent;
  let fixture: ComponentFixture<PostsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NzTableModule,
        NzDropDownModule,
        NzModalModule
      ],
      declarations: [
        PostsTableComponent,
        MockComponent(ShowPostComponent)
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsTableComponent);
    component = fixture.componentInstance;
    component.posts = postsStub;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should should display posts in table', () => {
    const [ firstPost ] = postsStub;
    const row = fixture.debugElement.query(By.css('.posts-table__row'))?.nativeElement;
    const firstTableCellContent = row.firstChild.innerHTML;
    expect(firstTableCellContent).toBe(firstPost?.title);
  });
});
