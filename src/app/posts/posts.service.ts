import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Post, NewPost } from '@posts/posts.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  readonly API_URL = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  getPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(`${this.API_URL}/api/v1/posts`);
  }

  getPost(postId: number): Observable<Post> {
    return this.httpClient.get<Post>(`${this.API_URL}/api/v1/posts/${postId}`);
  }

  createPost(post: NewPost): Observable<Post> {
    return this.httpClient.post<Post>(`${this.API_URL}/api/v1/posts`, post);
  }

  updatePost(post: Post, postId: number): Observable<Post> {
    return this.httpClient.put<Post>(`${this.API_URL}/api/v1/posts/${postId}`, post);
  }

  removePost(postId: number): Observable<Post> {
    return this.httpClient.delete<Post>(`${this.API_URL}/api/v1/posts/${postId}`);
  }

  showPost(postId: number): Observable<Post> {
    return this.httpClient.get<Post>(`${this.API_URL}/api/v1/posts/${postId}`);
  }
}
