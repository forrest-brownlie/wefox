import { createAction, props } from '@ngrx/store';
import { Post, NewPost } from '@posts/posts.model';

export class PostsActions {
  static listPosts = createAction('[Posts] List');
  static listPostsSuccess = createAction('[Posts] List success', props<{ list: Post[] }>());
  static listPostsFail = createAction('[Posts] List fail');
  static showPost = createAction('[Posts] Show', props<{ postId: number }>());
  static showPostSuccess = createAction('[Posts] Show success', props<{ post: Post }>());
  static showPostFail = createAction('[Posts] Show fail');
  static removeShowPost = createAction('[Posts] Show remove');
  static createPost = createAction('[Posts] Create', props<{ post: NewPost, closeDrawer: () => void }>());
  static createPostSuccess = createAction('[Posts] Create success', props<{ post: Post }>());
  static createPostFail = createAction('[Posts] Create fail');
  static updatePost = createAction('[Posts] Update', props<{ post: Post, postId: number, closeDrawer: () => void }>());
  static updatePostSuccess = createAction('[Posts] Update success', props<{ post: Post }>());
  static updatePostFail = createAction('[Posts] Update fail');
  static removePost = createAction('[Posts] Remove', props<{ postId: number }>());
  static removePostSuccess = createAction('[Posts] Remove success', props<{ removedPostId: number }>());
  static removePostFail = createAction('[Posts] Remove fail');
}
