import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed, waitForAsync } from '@angular/core/testing';
import { PostsService } from './posts.service';
import { RouterTestingModule } from '@angular/router/testing';
import { postsStub, updatedPostStub } from '../test-util/posts.stub';

describe('Posts Service', () => {
  let httpMock: HttpTestingController;
  let serviceMock: PostsService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, RouterTestingModule],
        providers: [PostsService],
      });
      httpMock = TestBed.inject(HttpTestingController);
      serviceMock = TestBed.inject(PostsService);
    })
  );

  it('should be created', inject([PostsService], (service: PostsService) => {
    expect(service).toBeTruthy();
  }));

  it('should get posts', () => {
    const dummyPosts = postsStub;

    serviceMock.getPosts().subscribe(posts => {
      expect(posts.length).toBe(3);
      expect(posts).toEqual(dummyPosts);
    });

    const req = httpMock.expectOne(`${serviceMock.API_URL}/api/v1/posts`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyPosts);
  });

  it('should get a post', () => {
    const [ dummyPost ] = postsStub;
    const { id } = dummyPost;

    serviceMock.getPost(id).subscribe(post => {
      expect(post).toEqual(dummyPost);
    });

    const req = httpMock.expectOne(`${serviceMock.API_URL}/api/v1/posts/${id}`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyPost);
  });

  it('should create a post', () => {
    const [ dummyPost ] = postsStub;
    const { id, ...newPost } = dummyPost;

    serviceMock.createPost(newPost).subscribe();

    const req = httpMock.expectOne(`${serviceMock.API_URL}/api/v1/posts`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toMatchObject(newPost);
    req.flush(newPost);
  });

  it('should update a post', () => {
    const { id } = updatedPostStub;
    serviceMock.updatePost(updatedPostStub, id).subscribe();

    const req = httpMock.expectOne(`${serviceMock.API_URL}/api/v1/posts/${id}`);
    expect(req.request.method).toBe('PUT');
    expect(req.request.body).toMatchObject(updatedPostStub);
    req.flush(updatedPostStub);
  });

  it('should remove a post', () => {
    const [ postToRemove ] = postsStub;

    serviceMock.removePost(postToRemove?.id).subscribe();

    const req = httpMock.expectOne(`${serviceMock.API_URL}/api/v1/posts/${postToRemove?.id}`);
    expect(req.request.method).toBe('DELETE');
    req.flush(updatedPostStub);
  });

  afterEach(() => {
    httpMock.verify();
  });
});
