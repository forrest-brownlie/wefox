import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NzTableModule } from 'ng-zorro-antd/table';
import { EffectsModule } from '@ngrx/effects';
import { ReactiveFormsModule } from '@angular/forms';
import { PostsEffects } from '@posts/posts.effects';
import { PostsComponent } from './components/posts/posts.component';
import { PostsTableComponent } from './components/posts-table/posts-table.component';
import { PostsTableContainerComponent } from './components/posts-table-container/posts-table-container.component';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { FEATURE_POSTS, postsReducer } from '@posts/posts.reducers';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { IconsProviderModule } from '../icons/icons.providers.module';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { PostFormComponent } from './components/post-form/post-form.component';
import { PostFormDrawerComponent } from './components/post-form-drawer/post-form-drawer.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ShowPostComponent } from './components/show-post/show-post.component';
import { PostMapComponent } from './components/post-map/post-map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    NzTableModule,
    NzDropDownModule,
    NzDrawerModule,
    NzModalModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    EffectsModule.forFeature([PostsEffects]),
    StoreModule.forFeature(FEATURE_POSTS, postsReducer),
    IconsProviderModule,
    ReactiveFormsModule,
    LeafletModule,
  ],
  declarations: [
    PostsTableContainerComponent,
    PostsTableComponent,
    PostsComponent,
    PostFormComponent,
    PostFormDrawerComponent,
    ShowPostComponent,
    PostMapComponent,
  ],
  providers: [],
})
export class PostsModule { }
