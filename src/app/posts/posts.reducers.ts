import { PostsActions } from '@posts/posts.actions';
import { Post } from '@posts/posts.model';
import { Action, createReducer, on } from '@ngrx/store';

export const FEATURE_POSTS = 'posts';

export interface PostsState {
  list: Post[];
  show: Post;
}

export const initialState: PostsState = {
  list: [],
  show: null,
};

const reducer = createReducer(
  initialState,
  on(PostsActions.listPostsSuccess, (state, { list }) => ({
    ...state,
    list,
  })),
  on(PostsActions.showPostSuccess, (state, { post }) => ({
    ...state,
    show: post,
  })),
  on(PostsActions.removeShowPost, (state) => ({
    ...state,
    show: null,
  })),
  on(PostsActions.createPostSuccess, (state, { post }) => ({
    ...state,
    list: [...state.list, post]
  })),
  on(PostsActions.removePostSuccess, (state, { removedPostId }) => ({
    ...state,
    list: state.list.filter(post => post?.id !== removedPostId)
  })),
  on(PostsActions.updatePostSuccess, (state, { post: updatedPost }) => {
    const updateIndex = state?.list?.map(post => post?.id).indexOf(updatedPost?.id);
    const updatedPosts = Object.assign([], state?.list, { [updateIndex]: updatedPost });
    return {
      ...state,
      list: updatedPosts
    };
  }),
);

export function postsReducer(state: PostsState = initialState, action: Action): PostsState {
  return reducer(state, action);
}
