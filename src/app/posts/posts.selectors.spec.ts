import { postsStub, singlePostStub } from '../test-util/posts.stub';
import * as postsSelectors from './posts.selectors';
import { initialState } from '@posts/posts.reducers';

describe('Posts selectors', () => {
  it('should return list of posts', () => {
    const dummyState = {
      ...initialState,
      list: postsStub,
    };
    expect(postsSelectors.getPosts.projector(dummyState)).toEqual(postsStub);
  });

  it('should return show post', () => {
    const dummyState = {
      ...initialState,
      show: singlePostStub,
    };
    expect(postsSelectors.getShowPost.projector(dummyState)).toEqual(singlePostStub);
  });
});
