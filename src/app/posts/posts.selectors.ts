import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FEATURE_POSTS, PostsState } from '@posts/posts.reducers';

const getPostsState = createFeatureSelector<PostsState>(FEATURE_POSTS);
export const getPosts = createSelector(getPostsState, state => state?.list);
export const getShowPost = createSelector(getPostsState, state => state?.show);
