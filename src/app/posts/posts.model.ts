export interface Post {
  id: number;
  title: string;
  content: string;
  lat: string;
  long: string;
  imageUrl: string;
  createdAt: string;
  updatedAt: string;
}

export interface NewPost {
  title: string;
  content: string;
  lat: string;
  long: string;
  imageUrl: string;
}
