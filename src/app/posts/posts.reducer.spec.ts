import { PostsActions } from '@posts/posts.actions';
import { initialState, postsReducer, PostsState } from '@posts/posts.reducers';
import { postsStub, singlePostStub } from '../test-util/posts.stub';

describe('Posts reducer', () => {
  it('initializes state', () => {
    const action = {} as any;
    const newState = postsReducer(undefined, action);

    expect(newState).toBe(initialState);
  });

  it('should return the list of posts on PostsActions.listPostsSuccess', () => {
    const action = PostsActions.listPostsSuccess({ list: postsStub });
    const newState = postsReducer(initialState, action);

    expect(newState.list).toEqual(postsStub);
  });

  it('should update the post on PostsActions.updatePostSuccess', () => {
    const updatedPostIndex = 0;
    const postToUpdate = postsStub[updatedPostIndex];
    const initState = {
      ...initialState,
      list: postsStub,
    };
    const updatedPost = {
      ...postToUpdate,
      title: 'bar',
    };
    const updatedPosts = Object.assign([], postsStub, { [updatedPostIndex]: updatedPost });
    const action = PostsActions.updatePostSuccess({ post: updatedPost });
    const newState = postsReducer(initState, action);

    expect(newState.list).toEqual(updatedPosts);
  });

  it('should add the post on PostsActions.createPostSuccess', () => {
    const action = PostsActions.createPostSuccess({ post: singlePostStub });
    const state: PostsState = {
      ...initialState,
      list: [singlePostStub]
    };
    const newState = postsReducer(initialState, action);
    expect(newState).toEqual(state);
  });

  it('should add the post on PostsActions.showPostSuccess', () => {
    const action = PostsActions.showPostSuccess({ post: singlePostStub });
    const state: PostsState = {
      ...initialState,
      show: singlePostStub
    };
    const newState = postsReducer(initialState, action);
    expect(newState).toEqual(state);
  });

  it('should remove the post on PostsActions.removeShowPost', () => {
    const action = PostsActions.removeShowPost();
    const state: PostsState = {
      ...initialState,
      show: null
    };
    const newState = postsReducer(initialState, action);
    expect(newState).toEqual(state);
  });

  it('should remove the post on PostsActions.removePostSuccess', () => {
    const [ postToRemove ] = postsStub;
    const { id: removedPostId } = postToRemove;
    const action = PostsActions.removePostSuccess({ removedPostId });
    const removePostInitialState = {
      ...initialState,
      list: postsStub
    };
    const state: PostsState = {
      ...initialState,
      list: postsStub.filter(post => post?.id !== removedPostId)
    };
    const newState = postsReducer(removePostInitialState, action);
    expect(newState).toEqual(state);
  });
});
