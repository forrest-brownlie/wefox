import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { PostsActions } from '@posts/posts.actions';
import { PostsService } from '@posts/posts.service';
import { mapKeys, camelCase, snakeCase } from 'lodash-es';
import { Post } from '@posts/posts.model';

function mapKeysToCamelCase(post: any): Post {
  return mapKeys(post, (v, k) => camelCase(k)) as Post;
}

function mapKeysToSnakeCase(post: any): Post {
  return mapKeys(post, (v, k) => snakeCase(k)) as Post;
}

@Injectable()
export class PostsEffects {

  constructor(private actions$: Actions, private postsService: PostsService) {}

  loadPosts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PostsActions.listPosts),
      switchMap(() => this.postsService.getPosts().pipe(
        map(list => {
          const camelCaseList = list.map(post => mapKeysToCamelCase(post));
          return PostsActions.listPostsSuccess({ list: camelCaseList });
        }),
        catchError(() => of(PostsActions.listPostsFail))
      ))
    )
  );

  createPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PostsActions.createPost),
      switchMap(({ post, closeDrawer }) => {
        const createPost = mapKeysToSnakeCase(post);
        return this.postsService.createPost(createPost).pipe(
          map(newPost => {
            closeDrawer();
            return PostsActions.createPostSuccess({ post: newPost });
          }),
          catchError(() => of(PostsActions.createPostFail()))
        );
      })
    )
  );

  updatePost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PostsActions.updatePost),
      switchMap(({ post, postId, closeDrawer }) => {
        const snakeCasePost = mapKeysToSnakeCase(post);
        return this.postsService.updatePost(snakeCasePost, postId).pipe(
          map(updatedPost => {
            closeDrawer();
            const camelCasePost = mapKeysToCamelCase(updatedPost);
            return PostsActions.updatePostSuccess({ post: camelCasePost });
          }),
          catchError(() => of(PostsActions.updatePostFail()))
        );
      })
    )
  );

  removePost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PostsActions.removePost),
      switchMap(({ postId }) => this.postsService.removePost(postId).pipe(
        map(() => PostsActions.removePostSuccess({ removedPostId: postId })),
        catchError(() => of(PostsActions.updatePostFail()))
      ))
    )
  );

  showPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PostsActions.showPost),
      switchMap(({ postId }) => this.postsService.showPost(postId).pipe(
        map(post => {
          const camelCasePost = mapKeysToCamelCase(post);
          return PostsActions.showPostSuccess({ post: camelCasePost });
        }),
        catchError(() => of(PostsActions.showPostFail()))
      ))
    )
  );
}
