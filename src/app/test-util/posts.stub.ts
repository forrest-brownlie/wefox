import { Post } from '@posts/posts.model';

export const postsStub: Post[] = [{
  id: 1,
  title: 'Madrid',
  content: 'Madrid is the capital of Spain and the largest municipality in both the Community of Madrid and Spain as a whole.',
  lat: '40.41678',
  long: '-3.70379',
  imageUrl: 'https://c2.staticflickr.com/2/1269/4670777817_d657cd9819_b.jpg',
  createdAt: '2020-04-24T11:43:34.020Z',
  updatedAt: '2020-04-24T11:43:34.020Z'
}, {
  id: 2,
  title: 'Barcelona',
  content: 'Barcelona is the capital and largest city of Catalonia with a population of 1.6 million within city limits.',
  lat: '41.3851',
  long: '2.1734',
  imageUrl: 'https://static.independent.co.uk/s3fs-public/styles/story_medium/public/thumbnails/image/2017/05/17/15/barcelona-skyline.jpg',
  createdAt: '2020-04-24T11:43:34.047Z',
  updatedAt: '2020-04-24T11:43:34.047Z'
}, {
  id: 3,
  title: 'Berlin',
  content: 'Berlin is the capital and the largest city of Germany as well as one of its 16 constituent states. With a population of approximately 3.7 million, Berlin is the second...',
  lat: '52.5065133',
  long: '13.1445548',
  imageUrl: 'https://lonelyplanetwp.imgix.net/2015/12/brandenburg-gate-berlin-GettyRF-1500-cs.jpg',
  createdAt: '2020-04-24T11:43:34.069Z',
  updatedAt: '2020-04-24T11:43:34.069Z'
}];

export const updatedPostStub: Post = {
  id: 1,
  title: 'Auckland',
  content: 'Auckland is the capital of Spain and the largest municipality in both the Community of Madrid and Spain as a whole.',
  lat: '40.41678',
  long: '-3.70379',
  imageUrl: 'https://c2.staticflickr.com/2/1269/4670777817_d657cd9819_b.jpg',
  createdAt: '2020-04-24T11:43:34.020Z',
  updatedAt: '2020-04-24T11:43:34.020Z'
};

export const singlePostStub: Post = {
  id: 1,
  title: 'Auckland',
  content: 'Auckland is the capital of Spain and the largest municipality in both the Community of Madrid and Spain as a whole.',
  lat: '40.41678',
  long: '-3.70379',
  imageUrl: 'https://c2.staticflickr.com/2/1269/4670777817_d657cd9819_b.jpg',
  createdAt: '2020-04-24T11:43:34.020Z',
  updatedAt: '2020-04-24T11:43:34.020Z'
};

